/* global app */

document.addEventListener("DOMContentLoaded", init);

function init() {
    app.initialized()
        .then(initApp)
        .catch(error => {
            console.error("[ERROR]", error);
        });
}

function initApp(client) {
    // client = Freshdesk object used in all App SDK documentation
    console.info("--- App initialized ---");
}
