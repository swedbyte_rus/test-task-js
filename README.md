## Interview Test App

### Task

* Each ticket requester is a contact in Freshdesk, but not every contact in Freshdesk is an agent.
* Clone this repo and create a table in a Full Page app that will contain a full name of each ticket author and a tick (✔) if this author is an agent (check the [./result.png](https://bitbucket.org/swedbyte_rus/test-task-js/src/master/result.png)).
* Bonus points: show a [toast notification](https://developers.freshdesk.com/v2/docs/interface-api/#ticketshownotify) in right top corner once the table is rendered (e.g. "Done!").
* Only up to last 30 tickets should be taken into consideration.
* The only file to work in - `./app/app.js`.
* Use `fdk run` command to run the app and then open [this URL](https://swedbyte.freshdesk.com/a/apps/testtaskjs?dev=true). Warning: make sure that you've added the Freshdesk account name to Chrome's security exceptions ([guide](https://developers.freshdesk.com/v2/docs/quick-start/#test_your_app)).
* All actions must be executed after the app is initialized (`initApp()` function).
* No frameworks, just ES6 JavaScript. You may use jQuery if you wish.
* For API requests use any method/library you prefer (e.g. `fetch()` or Freshdesk's own [Request API](https://developer.freshdesk.com/v2/docs/request-api/)).
* **Delivery**: Once done, archive the project folder and send it by mail or upload to Cloud Storage (Yandex.Disk/Google.Drive/Dropbox/etc) and share the link to archive.

### Resources

* [Pre-requisites setup guide](https://developer.freshdesk.com/v2/docs/quick-start/)
* [API Documentation](https://developer.freshdesk.com/api/)
* No restrictions related to web search - Google, GitHub, StackOverflow, etc.

### Credentials

* [API Requests headers](https://developer.freshdesk.com/api/#getting-started)
* Authorization header sample: `{ "Authorization": "Basic " + btoa("insert-API-Key-here" + ":x") }`
* Account credentials and API key provided in the email with task

### Folder structure explained

    .
    ├── README.md                  This file
    ├── app                        Contains the files that are required for the front end component of the app
    │   ├── app.js                 JS to render the dynamic portions of the app
    │   ├── icon.svg               Sidebar icon SVG file. Should have a resolution of 64x64px.
    │   ├── freshdesk_logo.png     The Freshdesk logo that is displayed in the app
    │   ├── style.css              Style sheet for the app
    │   ├── template.html          Contains the HTML required for the app’s UI
    ├── config                     Contains the installation parameters and OAuth configuration
    │   ├── iparams.json           Contains the parameters that will be collected during installation
    │   └── iparam_test_data.json  Contains sample Iparam values that will used during testing
    └── manifest.json              Contains app meta data and configuration information
